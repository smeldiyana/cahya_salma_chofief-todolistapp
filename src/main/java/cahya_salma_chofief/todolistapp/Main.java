
package cahya_salma_chofief.todolistapp;
import java.util.Scanner; 

public class Main 
{
    public static void main(String[] args) 
    {
        TodoListApp todolistapp = new TodoListApp();

        while(true){
            int choice = todolistapp.menu();
            todolistapp.executeMenu(choice);
        }
    }
}

/* 
javac cahya_salma_chofief/todolistapp/*.java && java cahya_salma_chofief.todolistapp.Main 
*/