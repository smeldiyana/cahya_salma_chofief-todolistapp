
package cahya_salma_chofief.todolistapp;

import org.junit.Test;
import static org.junit.Assert.*;

public class TaskListTest
{
    /* TaskList - add methods */
    @Test public void testAddTask1()
    {
        TaskList taskList = new TaskList();
        Task task1 = new Task("Task Name", "Description", 'I');

        taskList.add(task1);
     
        Task task2 = taskList.get(0);
        assertEquals(task1, task2);
    }
    @Test public void testAddTask2()
    {
        TaskList taskList = new TaskList();

        String name1 = "Task Name";
        String description1 = "Description";
        char status1 = 'I';

        taskList.add(name1, description1, status1);

        String name2 = taskList.get(0).getName();
        String description2 = taskList.get(0).getDescription();
        char status2 = taskList.get(0).getStatus();

        assertEquals(name1, name2);
        assertEquals(description1, description2);
        assertEquals(status1, status2);
    }
}
