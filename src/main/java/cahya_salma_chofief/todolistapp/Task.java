
package cahya_salma_chofief.todolistapp;

public class Task {
    private String name;
    private String description;
    private char status; // 'C' = Complete, 'I' = Incompete

    public Task(String name, String description, char status)
    {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public char getStatus(){
        return status;
    }

    public void setStatus(char status){
        this.status = status;
    }

    public void print(){
        System.out.println(name + " - " + description);
        System.out.println("Status : " + status);
    }

}
