
package cahya_salma_chofief.todolistapp;

import org.junit.Test;
import static org.junit.Assert.*;

public class TaskTest
{
    /* Task - get methods */
    @Test public void testTaskGetStatus() {
        Task task1 = new Task("Task Name", "Task Description", 'I');
        Task task2 = new Task("Task Name", "Task Description", 'C');
        assertEquals('I', task1.getStatus());
        assertEquals('C', task2.getStatus());
    
    }
    @Test public void testTaskGetName() {
        String name = "Task Name";
        Task task = new Task(name, "Task Description", 'I');
        assertEquals(name, task.getName());
    }
    @Test public void testTaskGetDescription() {
        String description = "Task Description";
        Task task = new Task("Task Name", description, 'I');
        assertEquals(description, task.getDescription());
    }

    /* Task - set methods */
    @Test public void testTaskSetStatus() {
        Task task = new Task("Task Name", "Task Description", 'I');
        task.setStatus('C');
        assertEquals('C', task.getStatus());
    
    }
    @Test public void testTaskSetName() {
        Task task = new Task("Task Name", "Task Description", 'I');
        String name = "New Name";
        task.setName(name);
        assertEquals(name, task.getName());
    }
    @Test public void testTaskSetDescription() {
        Task task = new Task("Task Name", "Task Description", 'I');
        String description = "New Description";
        task.setDescription(description);
        assertEquals(description, task.getDescription());
    }
}
