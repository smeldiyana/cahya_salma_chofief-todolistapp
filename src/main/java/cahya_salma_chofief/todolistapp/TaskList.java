
package cahya_salma_chofief.todolistapp;

import java.util.ArrayList;

public class TaskList {
    private ArrayList<Task> taskArray;

    public TaskList(){
        taskArray = new ArrayList<Task>();
    }

    public void add(String name, String description, char status){
        Task task = new Task(name, description, status);
        taskArray.add(task);
    }

    public void add(Task task){
        taskArray.add(task);
    }

    public Task get(int index){
        return taskArray.get(index);
    }

    public void printAll(){
        // print all tasks
        int i = 1;
        for (Task task : taskArray){
            System.out.println("");
            System.out.println("Task " + i + ":");
            task.print();
            i++;
        }
    }
}
