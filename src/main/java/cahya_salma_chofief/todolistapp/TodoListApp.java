
package cahya_salma_chofief.todolistapp;
import java.util.Scanner; 

public class TodoListApp 
{
    TaskList taskList;
    Scanner scanner;

    public TodoListApp(){
        taskList = new TaskList();
        taskList.add("Love Lisa", "She is the best", 'I');
        taskList.add("Loving on Lisa", "Write her a letter!", 'I');
        taskList.add("Call my parents", "Call my parents, they need to hear from me!", 'I');
        taskList.add("Git r done", "Learn Ruby, Rails, Javascript, NodeJS, React, Angular, Git, Command Line, and others as needed!!", 'I');
        taskList.add("Learn TDD", "Learn TDD", 'C');
        scanner = new Scanner(System.in);
    }

    public int menu() {
        System.out.println("");
        System.out.println("Welcome! Here are the menu options : ");
        System.out.println("1. Add to do item");
        System.out.println("2. Show to do item");
        System.out.println("What would you like to do? : ");

        int choice = scanner.nextInt();
        scanner.nextLine();

        return choice;
    }

    public void executeMenu(int choice) {
        switch(choice) {
            case 1: {
                System.out.println("");
                System.out.println("What is the task name? : ");
                String name = scanner.nextLine();

                System.out.println("");
                System.out.println("What is the task description? : ");
                String description = scanner.nextLine();           

                char status = 'A';
                do{
                    System.out.println("");
                    System.out.println("What is the task status? ('I' = incomplete, 'C' = complete) : ");
                    status = scanner.next().charAt(0);  
                }while(!(status == 'C' || status == 'I'));

                taskList.add(name, description, status);
                break;
            }
            case 2: taskList.printAll(); break;
            default: break;
        }
    }
}